import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from '@angular/forms';
@Component({
   selector: 'app-reactive-forms',
   templateUrl: './reactive-forms.component.html',
   styleUrls: ['./reactive-forms.component.scss']
})
export class ReactiveFormsComponent implements OnInit {
   locations: string[] = ['Downtown', 'S.Bay', 'Lakeside'];
   volunteerForm!: FormGroup;
   @Input() onForm: boolean = false;
   constructor(private fb: FormBuilder) { }

   ngOnInit(): void {
      this.initializeForm();
   }
   initializeForm() {
      this.volunteerForm = this.fb.group({
         name: '',
         phoneNumber: new FormControl('0991112233', Validators.pattern("[0-9 ]{10}")),
         preferredLocation: '',
         animals: this.fb.group({
            dogs: false,
            cats: false,
            reptiles: false
         }),
         references: this.fb.array([this.fb.control('')])
      })

   }
   onSubmit(): void {
      console.log(this.volunteerForm);
   }


   selectLocation(event: any): void {
      this.volunteerForm.patchValue({
         preferredLocation: event.target.value
      })
   }
   addEmail() {
      this.references.push(this.fb.control(''));
   }
   removeEmail(index: number) {
      this.references.removeAt(index);
   }


   get references(): FormArray {
      return this.volunteerForm.get('references') as FormArray;
   }
}

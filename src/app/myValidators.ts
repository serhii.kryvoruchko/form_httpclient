import { AbstractControl } from "@angular/forms"
import { Observable } from "rxjs"

export class myValidators {
   static restEmails(control: AbstractControl): { [key: string]: boolean } | null {
      if (['i@.mail.ru', '111@mail.ua'].includes(control.value)) {
         return {
            'restEmails': true
         }
      }
      return null
   }

   static zanatEmail(control: AbstractControl): Promise<any> | Observable<any> {
      return new Promise(resolve => {

         setTimeout(() => {
            if (control.value === 'async@mail.com') {
               resolve({ zanatEmail: true })
            }
            else {
               resolve(null)
            }
         }, 2000)

      })
   }
}
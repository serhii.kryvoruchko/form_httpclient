import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { myValidators } from './myValidators';


@Component({
   selector: 'app-root',
   templateUrl: './app.component.html',
   styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
   form!: FormGroup
   onForm: boolean = false

   constructor() {

   }
   ngOnInit() {
      this.form = new FormGroup({
         email: new FormControl('', [Validators.email, Validators.required, myValidators.restEmails], myValidators.zanatEmail),
         password: new FormControl(null, [Validators.required, Validators.minLength(6)]),
         address: new FormGroup({
            country: new FormControl('ua'),
            city: new FormControl('Киев', Validators.required)
         }),
         skills: new FormArray([])
      })

   }
   submit() {
      if (this.form.valid) {
         const formData = { ...this.form.value }
         console.log('form Data', formData)
         this.form.reset()
      }
   }
   get skillControls() {
      return (this.form.get('skills') as FormArray).controls;

   }


   setCapital() {
      const cityMap: any = {
         ru: 'Москва',
         by: 'Минск',
         ua: 'Харьков'
      }
      const cityKey = this.form.get('address')?.get('country')?.value;
      const city = cityMap[cityKey];

      this.form.patchValue({
         address: { city }
      })
   }
   // get skills() {
   //    return this.form.controls["lessons"] as FormArray

   // }
   addSkills() {
      const control = new FormControl('', Validators.required);
      (this.form.get('skills') as FormArray).push(control)
   }
   offForm() {
      this.onForm = !this.onForm
      console.log(this.onForm);

   }
}

import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { catchError, delay } from "rxjs/operators";

export interface Todo {
   completed: boolean
   title: string
   id?: number
}

@Injectable({ providedIn: 'root' })
export class TodosService {
   url: string = 'https://jsonplaceholder.typicode.com/todos'
   constructor(private http: HttpClient) { }

   addTodo(todo: Todo): Observable<Todo> {
      const headers = new HttpHeaders({
         'myPhoneNumber': '0638868153',
      })
      return this.http.post<Todo>(this.url, todo, { headers })
   }

   fetchTodo(): Observable<Todo[]> {
      let params = new HttpParams()
      params = params.append('_limit', '3')
      params.append('custom', 'anything')
      return this.http.get<Todo[]>(this.url, {
         params
      })
         .pipe(
            delay(500),
            catchError(err => {
               console.log('Ошибочка: ', err.massage);
               return throwError(err)
            }),

         )
   }
   removeTodo(id: number): Observable<void> {
      return this.http.delete<void>(`${this.url}/${id}`)
   }

   completeTodo(id: number): Observable<Todo> {
      return this.http.put<Todo>(`${this.url}/${id}`, {
         completed: true
      })
   }
}

import { Component, Input, OnInit } from '@angular/core';
import { Todo, TodosService } from '../todos.service';


@Component({
   selector: 'app-http-client',
   templateUrl: './http-client.component.html',
   styleUrls: ['./http-client.component.scss']
})
export class HttpClientComponent implements OnInit {

   todos: Todo[] = []
   loading = false
   todoTitle = ''
   error = ''


   @Input() onForm: boolean = true

   constructor(private todosService: TodosService) { }

   ngOnInit(): void {
   }


   addTodo() {
      if (!this.todoTitle.trim()) {
         return
      }
      this.todosService.addTodo({
         title: this.todoTitle,
         completed: false
      }).subscribe(todo => {
         this.todos.push(todo)

         this.todoTitle = ''
      })

   }


   fetchTodo() {
      this.loading = true
      this.todosService.fetchTodo()
         .subscribe(todos => {
            this.todos = todos
            this.loading = false
         }, error => {
            console.log('ERRRRROR : ', error.message)
            this.error = error.massage
         })
   }
   removeTodo(id: number) {
      this.todosService.removeTodo(id)
         .subscribe(() => {
            this.todos = this.todos.filter(t => t.id !== id)
         })
   }

   completeTodo(id: number) {
      this.todosService.completeTodo(id).subscribe(todo => {
         this.todos.find(t => t.id === todo.id)!.completed = true

      })
   }
}
